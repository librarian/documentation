> If you have any issues, join us on [Matrix](https://matrix.to/#/%23librarian:nitro.chat) or [open an issue](https://codeberg.org/librarian/librarian/issues/new).

# Librarian Docker Setup Guide
Docker is the recommended way to setup Librarian. This guide will show you step-by-step instructions for installation. This guide assumes that you have already setup your server.

> **⚠️ WARNING**: Most VPS providers use unsecure defaults. You should make sure you are using SSH key authentication, using a user account and not the root account, and have a firewall enabled. If you don't know what any of this means, stop and research these topics before continuing.

This install guide should work on:
- Arch Linux
- Debian/Ubuntu/ other Debian-based
- Fedora/RHEL/other Red Hat

## 1. Install Docker
Follow the instructions to install Docker on https://docs.docker.com/engine/install/

## 2. Download and setup Librarian
Make a new directory and create the `docker-compose.yml` file with the following contents:
```sh
mkdir librarian
nano docker-compose.yml
```

docker-compose.yml:
```yml
version: '3'

services:
  librarian:
    image: nineteengladespool/librarian:latest
    ports:
      - 3000:3000
    volumes:
      - ./config.yml:/app/config.yml
    restart: unless-stopped
```

Optionally, add stream-proxy to proxy videos and livestreams. This will require additional setup in the next step.
```yml
  stream-proxy:
    image: registry.gitlab.com/overtime-zone-wildfowl/stream-proxy-ng:latest
    ports:
      - 3001:3001
    restart: unless-stopped
```

Create the Librarian config file and paste in the following contents, then modify the config:
```
nano config.yml
```

config.yml:
```yml
DOMAIN: 'https://domain.tld'
PORT: '3000'
ADDRESS: '127.0.0.1'

# If you added stream-proxy, uncomment these lines:
#ENABLE_LIVE_STREAM: true
#VIDEO_STREAMING_URL: ''
#LIVE_STREAMING_URL: ''

# Instance privacy: This is required to get your instance listed. For more info,
# See: https://codeberg.org/librarian/librarian/wiki/Instance-privacy
INSTANCE_PRIVACY:
# This is the default if you are using NGINX and have not disabled data collection.
# Read https://codeberg.org/librarian/librarian/wiki/Instance-privacy
  DATA_NOT_COLLECTED: false
  DATA_COLLECTED_IP: true
  DATA_COLLECTED_URL: true
  DATA_COLLECTED_DEVICE: true
  DATA_COLLECTED_DIAGNOSTIC_ONLY: false
  # The country your instance is located. Leave blank if hosting on darknet.
  INSTANCE_COUNTRY: "United States"
  # Put the name of your hosting provider. If hosting from home, use the name of your ISP. Leave blank if hosting on darknet.
  INSTANCE_PROVIDER: "Amazon Web Services"
  # Cloudflare use is discouraged. You can set this to false if it is not proxied (gray cloud icon)
  INSTANCE_CLOUDFLARE: false
  # Optional: Explain your usage of data (if collected) and how it is stored.
  MESSAGE: ""
  # Link to your privacy policy, leave blank if you don't have one.
  PRIVACY_POLICY: ""
```

Start Librarian (and stream-proxy if added):
```sh
sudo docker-compose up -d
```

## 3. Setup Caddy
[Caddy](https://caddyserver.com/) is recommended and will simplify setup. If you are already using another reverse proxy, you can skip this step and configure it yourself.

Install Caddy using the instructions for your distro on https://caddyserver.com/docs/install

Then, edit `/etc/caddy/Caddyfile` and use the following (if /etc/caddy does not exist, create it with `sudo mkdir /etc/caddy`). Replace domain.tld with your domain:
```
sudo nano /etc/caddy/Caddyfile
```

/etc/caddy/Caddyfile:
```
librarian.domain.tld {
  log {
    output discard
  }

  # Remove if you didn't setup stream-proxy
  handle /live/* {
    reverse_proxy 127.0.0.1:3001
  }
  handle /stream/* {
    reverse_proxy 127.0.0.1:3001
  }

  reverse_proxy 127.0.0.1:3000
}
```

> **⚠️ WARNING**: You need to setup your DNS records to point to your server before starting Caddy. This depends on your DNS provider.

Start Caddy:
```sh
sudo systemctl enable --now caddy
```

🎉 Your Librarian instance should now be available at your domain!