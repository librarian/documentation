# Comments (comments.odysee.tv)
Odysee comments API.

## `POST` List comments
```
https://comments.odysee.tv/api/v2?m=comment.List
```

### `application/json` Body
* **top_level:** return replies
* **sort_by:** 3 - Best, 2 - Controversial, 0 - New
* **parent_id:** comment ID of the comment to get replies

```json
{
  "jsonrpc": "2.0",
  "id": 1,
  "method": "comment.List",
  "params": {
    "page": 1,
    "page_size": 10,
    "top_level": true,
    "sort_by": 3,
    "parent_id": "",
    "claim_id": "463e63afb35a319f260b36ef8d5c3dc41a98ce28",
    "channel_id": "ecf0a6be99030d0ad4e10aec11d2c0bab94246ae",
    "channel_name": "@MusicARetro"
  }
}
```

### `200 OK` Response
```json
{
  "jsonrpc": "2.0",
  "result": {
    "page": 1,
    "page_size": 600,
    "total_pages": 1,
    "total_items": 3,
    "total_filtered_items": 3,
    "items": [
      {
        "comment": "otimo",
        "comment_id": "cf646d91c2c98a9471954b63bc51cf8c78f86fa1c734fb0c65e49ed0ded3799b",
        "claim_id": "463e63afb35a319f260b36ef8d5c3dc41a98ce28",
        "timestamp": 1613902598,
        "signature": "",
        "signing_ts": "1613902598",
        "is_hidden": false,
        "is_pinned": false,
        "channel_id": "d177dcbe8cfd1b6e58797ea573ba856a2be759c0",
        "channel_name": "@joseavf1",
        "channel_url": "lbry://@joseavf1#d177dcbe8cfd1b6e58797ea573ba856a2be759c0",
        "support_amount": 0
      }, {
        "comment":"True. Always entertaining content.",
        "comment_id":"49357e258bf3b69574ead22e43144630c94ca73950b84cc7280f99c610b559a2",
        "claim_id":"93b951fc1f8c7af3def41073c539ec957c3c562a",
        "timestamp":1625274573,
        "parent_id":"fafae646a3c1ae2e3f4631b77733ef5a6e197f052228d8ae09aa642aa2ddff34",
        "signature":"",
        "signing_ts":"1625274572",
        "is_hidden":false,
        "is_pinned":false,
        "channel_id":"0db3cea9230c2506e266df053a5a8aca8dc39214",
        "channel_name":"@VoidHeart",
        "channel_url":"lbry://@VoidHeart#0db3cea9230c2506e266df053a5a8aca8dc39214",
        "replies":1,
        "support_amount":0
      }
    ],
    "has_hidden_comments": false
  },
  "id": 1
}
```

## `POST` Get comment reactions
```
https://comments.odysee.tv/api/v2?m=reaction.List
```

### `application/json` Body
* **comment_ids:** Comma-separated list of comment IDs

```json
{
  "jsonrpc": "2.0",
  "id": 1,
  "method": "reaction.List",
  "params": {
    "comment_ids": "commentId,commentId2"
    }
  }
```

### `200 OK` Response
```json
{
  "jsonrpc": "2.0",
  "result": {
    "others_reactions": {
      "062d9b51b099db7c4bba2e3aa2811badc901cf116f6fb6cdb68e0a636784fa14": {
        "": 0,
        "bones": 0,
        "creator_like": 0,
        "creators_like": 0,
        "dislike": 0,
        "frozen_tom": 0,
        "like": 1,
        "likes": 0,
        "mind_blown": 0
      }
    }
  },
  "id": 1
}
```