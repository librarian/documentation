# Search (lighthouse.odysee.tv)
Odysee search API.

## `GET` Search
```
https://lighthouse.odysee.tv/search
```
Search Odysee for channels and videos.

### Query parameters
| Key        | Required  | Values | Description | 
|------------|-----------|--------|-------------|
| s          | Yes       | string (3-99999 characters) | Search query |
| size       | No        | number, default: 9 | Amount of results to get |
| from       | No        | number | Use to get next page of results |
| claimType  | No        | accepts list: file, channel, default: file,channel | type of claim to search for |
| nsfw       | No        | bool (default: false)   | Show NSFW videos |
| free_only  | No        | bool   | | Show paid videos
| related_to | No        | string | | Claim ID to get related claims

### `200 OK` Response
```
[
  {
    "claimId": "2332c0df0bd5e046db04fe7e3e79f756d35a1523",
    "name": "@DistroTube"
  }
]
```

### `400 Bad Request` Response
```
{
  "success": false,
  "error": "S: cannot be blank.",
  "data": null
}
```

## `GET` Autocomplete
```
https://lighthouse.odysee.tv/autocomplete
```
Autocomplete search query

### Query parameters
| Key        | Required  | Values | Description | 
|------------|-----------|--------|-------------|
| s          | Yes       | string | Search query |


### `200 OK` Response
```
[
  "searchnu-searchqu-software-entfernen",
  "searchsploit-searching-for-exploits",
  "SearchFunctionHangUp",
  "search1",
  "searchblackbox-net-history",
  "CRYM---Searchlights",
  "searchblackbox-review-1",
  "torrent-search-searching-bittorrent",
  "searchthat-net-v7",
  "searchthat-new-design"
]
```

### `400 Bad Request` Response
```
{
  "success": false,
  "error": "S: cannot be blank.",
  "data": null
}
```